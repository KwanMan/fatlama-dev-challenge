module.exports = {
  port: 3000,
  // jsx: 'React.createElement' | 'h', // if react is installed, h otherwise
  client: './src', // the directory with the client code, ignored by nodemon
  // server: './app/server', // the directory with the server code
  static: './static' // if you want to serve assets like images
  // dist: 'dist', // the dir for building production client side code
  // html: './index.html', // if you want to change the default html served
  // hot: true, // toggle hot reloading,
  // quiet: false, // no jetpack logs, only errors and logs from the app
  // verbose: false // more detailed logs
}
