import axios from 'axios'
import createAtom from 'tiny-atom'
import uuid from 'uuid/v4'
import _ from 'lodash'

const ENDPOINT = 'http://localhost:8080'

export default function createState (render) {
  const initialState = {
    userEntities: {},
    transactionEntities: {},
    transactionList: [],
    loadingTransactions: false,
    actioning: false,
    notification: false
  }

  const actions = {
    showNotification (get, split, { type, text }) {
      const id = uuid()
      split({
        notification: { id, type, text, open: true }
      })
      setTimeout(() => {
        if (_.get(get(), 'notification.id') === id) {
          split({
            notification: {
              ...get().notification,
              open: false
            }
          })
        }
      }, 2000)
    },
    async fetchTransactions (get, split, page = 0) {
      try {
        const { data } = await axios.get(`${ENDPOINT}/transactions/${page}`)
        if (Array.isArray(data)) {
          if (!data.length) {
            return
          }
          const { transactionList, transactionEntities } = get()
          data.forEach(transaction => {
            transactionList.push(transaction.id)
            transactionEntities[transaction.id] = transaction
          })
          split({ transactionList, transactionEntities })
        }
      } catch (e) {
        split('showNotification', {
          type: 'warning',
          text: 'Error loading transactions'
        })
      }
      split('fetchTransactions', page + 1)
    },
    async fetchUser (get, split, userId) {
      const { data } = await axios.get(`${ENDPOINT}/user/${userId}`)
      split({
        userEntities: {
          ...get().userEntities,
          [userId]: data
        }
      })
    },
    async showTransaction (get, split, transactionId) {
      split({ openTransactionId: transactionId })
      const { transactionEntities, userEntities } = get()
      const { lenderId, borrowerId } = transactionEntities[transactionId]
      if (!userEntities[lenderId]) split('fetchUser', lenderId)
      if (!userEntities[borrowerId]) split('fetchUser', borrowerId)
    },
    async authorize (get, split, transactionId) {
      split({ actioning: true })
      let transaction
      try {
        const {
          data
        } = await axios.put(`${ENDPOINT}/transaction/${transactionId}`, {
          status: 'FL_APPROVED'
        })
        transaction = data
      } catch (e) {
        split('showNotification', {
          type: 'error',
          text: `Failed authorizing transaction #${transactionId}`
        })
        split({ actioning: false })
        return
      }
      const { transactionEntities } = get()
      transactionEntities[transactionId] = transaction
      split({ transactionEntities, actioning: false })
      split('showNotification', {
        type: 'success',
        text: `Successfully authorized transaction #${transactionId}`
      })
    },
    async quarantine (get, split, transactionId) {
      split({ actioning: true })
      let transaction
      try {
        const {
          data
        } = await axios.put(`${ENDPOINT}/transaction/${transactionId}`, {
          status: 'QUARANTINED'
        })
        transaction = data
      } catch (e) {
        split('showNotification', {
          type: 'error',
          text: `Failed quarantining transaction #${transactionId}`
        })
        split({ actioning: false })
        return
      }
      const { transactionEntities } = get()
      transactionEntities[transactionId] = transaction
      split({ transactionEntities, actioning: false })
      split('showNotification', {
        type: 'success',
        text: `Successfully quarantined transaction #${transactionId}`
      })
    }
  }

  function evolve (get, split, { type, payload }) {
    if (actions[type]) {
      actions[type](get, split, payload)
    } else {
      console.log(`No handler for ${type}`)
    }
  }

  return createAtom(initialState, evolve, render)
}
