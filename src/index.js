import injectStyles from './injectStyles'
import React from 'react'
import ReactDOM from 'react-dom'
import { ProvideAtom } from 'tiny-atom/react'
import App from './App'
import createState from './createState'

import 'flexboxgrid2'

injectStyles()

const atom = createState(render)

function render () {
  ReactDOM.render(
    <ProvideAtom atom={atom}><App /></ProvideAtom>,
    document.getElementById('root')
  )
}

render()

atom.split('fetchTransactions')

if (process.env.NODE_ENV === 'development') {
  window.atom = atom
}
