import React from 'react'
import _ from 'lodash'
import classnames from 'classnames'
import { formatDates } from '../utils'

import './TransactionRow.css'

export default function TransactionRow (props) {
  const { transaction, onSelect, selected } = props
  const { itemId, status } = transaction
  const className = classnames(
    'TransactionRow',
    `TransactionRow-${_.camelCase(status)}`,
    {
      isSelected: selected
    }
  )
  return (
    <div className={className} onClick={onSelect}>
      Item #{itemId} {formatDates(transaction)} ({status})
    </div>
  )
}
