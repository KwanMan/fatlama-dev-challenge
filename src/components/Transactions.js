import React from 'react'
import classnames from 'classnames'
import { connect } from 'tiny-atom/react'
import Select from 'react-select'
import _ from 'lodash'
import TransactionRow from './TransactionRow'

import './Transactions.css'

const map = (state, split) => {
  const transactions = [...state.transactionList]
    .sort((a, b) => b - a)
    .map(id => state.transactionEntities[id])
  return {
    transactions,
    onSelectTransaction: id => split('showTransaction', id),
    availableStatuses: _.uniq(transactions.map(t => t.status)),
    openTransactionId: state.openTransactionId
  }
}

class Transactions extends React.Component {
  constructor () {
    super()
    this.state = {
      statuses: ['ESCROW']
    }
  }

  render () {
    const {
      transactions,
      onSelectTransaction,
      availableStatuses,
      openTransactionId
    } = this.props
    const { statuses } = this.state
    return (
      <div className={classnames('Transactions', this.props.className)}>
        <div className='Transactions-inner'>
          <Select
            value={this.state.statuses.map(toOption)}
            onChange={statuses =>
              this.setState({ statuses: statuses.map(fromOption) })}
            options={availableStatuses.map(toOption)}
            isMulti
          />
          {transactions.map(transaction => {
            if (statuses.length && !statuses.includes(transaction.status)) {
              return null
            }
            return (
              <TransactionRow
                key={transaction.id}
                transaction={transaction}
                onSelect={() => onSelectTransaction(transaction.id)}
                selected={openTransactionId === transaction.id}
              />
            )
          })}
        </div>
      </div>
    )
  }
}

export default connect(map)(Transactions)

function toOption (v) {
  return {
    label: v,
    value: v
  }
}

function fromOption ({ value }) {
  return value
}
