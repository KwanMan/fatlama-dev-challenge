import React from 'react'
import classnames from 'classnames'
import { connect } from 'tiny-atom/react'

import './Header.css'

const map = state => {
  return {
    notification: state.notification
  }
}

function Header (props) {
  const { notification } = props

  const notificationClasses = {
    'Header-notification': true,
    isOpen: notification && notification.open
  }
  if (notification) {
    notificationClasses[`Header-notification-${notification.type}`] = true
  }

  return (
    <div className={classnames('Header', props.className)}>
      <div className='Header-logo'>
        <img src='/static/logo-text.png' />
        <img src='/static/logo-llama.png' />
      </div>
      <div className={classnames(notificationClasses)}>
        {notification ? notification.text : null}
      </div>
    </div>
  )
}

export default connect(map)(Header)
