import React from 'react'
import classnames from 'classnames'
import { connect } from 'tiny-atom/react'
import _ from 'lodash'
import UserInfo from './UserInfo'
import { formatDates, formatCurrency } from '../utils'

import './TransactionInfo.css'

const map = (state, split) => {
  const transaction = state.transactionEntities[state.openTransactionId]
  const lender = transaction && state.userEntities[transaction.lenderId]
  const borrower = transaction && state.userEntities[transaction.borrowerId]
  return {
    transaction,
    lender,
    borrower,
    authorize: () => split('authorize', state.openTransactionId),
    quarantine: () => split('quarantine', state.openTransactionId),
    actioning: state.actioning
  }
}

function TransactionInfo (props) {
  const {
    transaction,
    lender,
    borrower,
    actioning,
    authorize,
    quarantine
  } = props
  const className = classnames('TransactionInfo', props.className, {
    isEmpty: !transaction
  })
  return (
    <div className={className}>
      {transaction ? [renderTransaction(), renderActions()] : renderEmpty()}
    </div>
  )

  function renderEmpty () {
    return (
      <div className='TransactionInfo-emptyMessage'>Select a transaction</div>
    )
  }

  function renderActions () {
    if (transaction.status !== 'ESCROW') return null
    const className = classnames('TransactionInfo-actions', {
      isActioning: actioning
    })
    return (
      <div className={className}>
        <div className='TransactionInfo-authorize' onClick={authorize}>
          Authorize
        </div>
        <div className='TransactionInfo-quarantine' onClick={quarantine}>
          Quarantine
        </div>
      </div>
    )
  }

  function renderTransaction () {
    const { totalDiscount, promocode, creditUsed, status } = transaction
    const fields = [
      <div className='TransactionInfo-users'>
        <UserInfo loading={!lender} {...lender} party='Lender' />
        <UserInfo loading={!borrower} {...borrower} party='Borrower' />
      </div>,
      <div>Transaction #{transaction.id}</div>,
      <div>Item #{transaction.itemId}</div>,
      <div className='TransactionInfo-dates'>{formatDates(transaction)}</div>,
      <div className='TransactionInfo-price'>
        Price: {formatCurrency(transaction, transaction.price)}
      </div>
    ]

    if (totalDiscount) {
      fields.push(
        <div className='TransactionInfo-discounts'>
          Discounts:
        </div>
      )

      if (creditUsed) {
        fields.push(
          <div className='TransactionInfo-credit'>
            {formatCurrency(transaction, creditUsed)} credit
          </div>
        )
      }

      if (promocode) {
        const promoDiscount = formatCurrency(
          transaction,
          totalDiscount - creditUsed
        )
        fields.push(
          <div className='TransactionInfo-promocode'>
            {promoDiscount} (using promocode {promocode})
          </div>
        )
      }
    }

    fields.push(
      <div className='TransactionInfo-status'>
        <span>status: </span>
        <span className={`TransactionInfo-status-${_.camelCase(status)}`}>
          {status}
        </span>
      </div>
    )

    return fields
  }
}

export default connect(map)(TransactionInfo)
