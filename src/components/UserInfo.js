import React from 'react'
import classnames from 'classnames'

import './UserInfo.css'

function UserInfo (props) {
  const {
    loading,
    profileImgUrl,
    firstName,
    lastName,
    email,
    telephone,
    party
  } = props
  return (
    <div className={classnames('UserInfo', props.className)}>
      {loading ? renderLoader() : renderDetails()}
    </div>
  )

  function renderLoader () {
    return <div>Loading...</div>
  }

  function renderDetails () {
    return [
      <h3>{party}</h3>,
      <img className='UserInfo-profileImage' src={profileImgUrl} />,
      <div>{firstName} {lastName}</div>,
      <div>{email}</div>,
      <div>{telephone}</div>
    ]
  }
}

export default UserInfo
