import React from 'react'
import Header from './components/Header'
import Transactions from './components/Transactions'
import TransactionInfo from './components/TransactionInfo'

import './App.css'

export default function App () {
  return (
    <div className='App'>
      <Header className='App-header' />
      <div className='App-body container'>
        <Transactions className='col-xs-6' />
        <TransactionInfo className='col-xs-6' />
      </div>
    </div>
  )
}
