import moment from 'moment'
export function formatDates ({ fromDate, toDate }) {
  const from = moment(fromDate)
  const to = moment(toDate)
  const days = to.diff(from, 'days')
  return `from ${from.format('YYYY-MM-DD')} for ${days} day${days > 1 ? 's' : ''}`
}

const currencyMap = {
  GBP: '£'
}
export function formatCurrency ({ currency }, value) {
  return `${currencyMap[currency]} ${value.toFixed(2)}`
}
