import Typography from 'typography'

const typography = new Typography({
  googleFonts: [
    {
      name: 'Lato',
      styles: ['100', '300', '400', '400i']
    },
    {
      name: 'Lora',
      styles: ['400', '700']
    }
  ],
  // headerFontFamily: ['Lora', 'serif'],
  headerFontFamily: ['Lato', 'sans-serif'],
  headerColor: '#030d54',
  headerWeight: 'bold',

  bodyFontFamily: ['Lato', 'sans-serif'],
  bodyWeight: '100',
  baseFontSize: '12px',

  blockMarginBottom: 0
})

export default function injectStyles () {
  typography.injectStyles()
}
